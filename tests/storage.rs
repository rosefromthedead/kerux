use fs_err::tokio as fs;
use kerux_core::storage::{Storage, StorageManager};

#[tokio::test]
async fn mem_backend_user_accounts() {
    let db_pool = kerux_storage_mem::MemStorageManager::new();
    let db = db_pool.get_handle().await.unwrap();
    user_accounts(&*db).await;
}

#[tokio::test]
async fn sled_backend_user_accounts() {
    let path = "sled-test-user-accounts";
    let _ = fs::remove_dir_all(path).await;
    let db_pool = kerux_storage_sled::SledStorage::new(path).unwrap();
    let db = db_pool.get_handle().await.unwrap();
    user_accounts(&*db).await;
    let _ = fs::remove_dir_all(path).await;
}

async fn user_accounts(db: &dyn Storage) {
    db.create_user("alice", "password1")
        .await
        .expect("failed to create first user");
    db.create_user("alice", "password1")
        .await
        .expect_err("succeeded making same user twice");
    db.create_user("alice", "password2")
        .await
        .expect_err("succeeded making same user twice");
    db.create_user("bob", "password1")
        .await
        .expect("failed to create second user");

    assert!(db.verify_password("alice", "password1").await.unwrap() == true);
    assert!(db.verify_password("alice", "password2").await.unwrap() == false);
    assert!(db.verify_password("bob", "password1").await.unwrap() == true);
    assert!(db.verify_password("bob", "password2").await.unwrap() == false);

    let alice_token_1 = db
        .create_access_token("alice", "phone")
        .await
        .expect("failed to create token");
    let alice_token_2 = db
        .create_access_token("alice", "laptop")
        .await
        .expect("failed to create token");
    let bob_token_1 = db
        .create_access_token("bob", "laptop")
        .await
        .expect("failed to create token");
    let bob_token_2 = db
        .create_access_token("bob", "phone")
        .await
        .expect("failed to create token");
    db.create_access_token("nobody", "kjfnkfn")
        .await
        .expect_err("succeeded making token for nobody");

    db.delete_access_token(alice_token_2)
        .await
        .expect("failed to delete token");
    db.delete_access_token(bob_token_2)
        .await
        .expect("failed to delete token");

    assert_eq!(
        db.try_auth(alice_token_1)
            .await
            .expect("failed during auth")
            .map(|token_data| token_data.username)
            .as_deref(),
        Some("alice")
    );
    assert_eq!(
        db.try_auth(alice_token_2)
            .await
            .expect("failed during auth")
            .map(|token_data| token_data.username)
            .as_deref(),
        None
    );
    assert_eq!(
        db.try_auth(bob_token_1)
            .await
            .expect("failed during auth")
            .map(|token_data| token_data.username)
            .as_deref(),
        Some("bob")
    );
    assert_eq!(
        db.try_auth(bob_token_2)
            .await
            .expect("failed during auth")
            .map(|token_data| token_data.username)
            .as_deref(),
        None
    );

    db.delete_all_access_tokens(alice_token_1)
        .await
        .expect("failed to delete all tokens");
    assert_eq!(
        db.try_auth(alice_token_1)
            .await
            .expect("failed during auth")
            .map(|token_data| token_data.username)
            .as_deref(),
        None
    );
    assert_eq!(
        db.try_auth(bob_token_1)
            .await
            .expect("failed during auth")
            .map(|token_data| token_data.username)
            .as_deref(),
        Some("bob")
    );
}

#[tokio::test]
async fn mem_backend_transactions() {
    let db_pool = kerux_storage_mem::MemStorageManager::new();
    let db = db_pool.get_handle().await.unwrap();
    transactions(&*db).await;
}

#[tokio::test]
async fn sled_backend_transactions() {
    let path = "sled-test-transactions";
    let _ = fs::remove_dir_all(path).await;
    let db_pool = kerux_storage_sled::SledStorage::new(path).unwrap();
    let db = db_pool.get_handle().await.unwrap();
    transactions(&*db).await;
    let _ = fs::remove_dir_all(path).await;
}

async fn transactions(db: &dyn Storage) {
    db.create_user("alice", "password").await.unwrap();
    let token = db.create_access_token("alice", "phone").await.unwrap();
    assert_eq!(
        db.record_txn(token, String::from("txn1"))
            .await
            .expect("failed to record transaction"),
        true
    );
    assert_eq!(
        db.record_txn(token, String::from("txn1"))
            .await
            .expect("failed to record transaction"),
        false
    );
    assert_eq!(
        db.record_txn(token, String::from("txn2"))
            .await
            .expect("failed to record transaction"),
        true
    );
}
