use std::collections::HashMap;

use kerux_core::{
    auth::AuthStatus,
    error::Error,
    events::{
        pdu::StoredPdu,
        room::{Create, Member, Membership, Name, RoomVersion},
        room_version::{v4::UnhashedPdu, VersionedPdu},
        EventContent,
    },
    state::{NewEvent, StateResolver},
    storage::{Storage, StorageManager},
    MatrixId, RoomId,
};

struct TestRoom<'db> {
    db: &'db dyn Storage,
    room_id: RoomId,
    /// depth -> list of events at that depth
    depth_map: Vec<Vec<String>>,
}

impl<'a> TestRoom<'a> {
    /// must only be called once per test, because it uses the same room id every time
    async fn create<'db>(
        db: &'db dyn Storage,
        room_id: &RoomId,
        creator: &MatrixId,
    ) -> Result<TestRoom<'db>, Error> {
        let creation = UnhashedPdu {
            event_content: EventContent::Create(Create {
                creator: creator.clone(),
                room_version: Some(RoomVersion::V4),
                predecessor: None,
                extra: HashMap::new(),
            }),
            room_id: room_id.to_owned(),
            sender: creator.clone(),
            state_key: Some(String::new()),
            unsigned: None,
            redacts: None,
            origin: "example.org".parse().unwrap(),
            origin_server_ts: 0,
            prev_events: Vec::new(),
            depth: 0,
            auth_events: Vec::new(),
        }
        .finalize();
        let creation_id = creation.event_id();
        db.add_pdus(&[StoredPdu {
            inner: VersionedPdu::V4(creation),
            auth_status: AuthStatus::Pass,
        }])
        .await?;
        Ok(TestRoom {
            db,
            room_id: room_id.to_owned(),
            depth_map: vec![vec![creation_id]],
        })
    }

    /// Like `crate::state::add_event`, but it allows you to specify depth,
    /// which you usually don't want, but is very useful for testing.
    async fn add(
        &mut self,
        depth: usize,
        sender: &MatrixId,
        content: impl Into<EventContent>,
        state_key: Option<&str>,
        state_resolver: &StateResolver,
    ) -> Result<String, Error> {
        let prev_depth = depth.checked_sub(1).unwrap();
        let prev_events = &self.depth_map[prev_depth];
        let state = state_resolver.resolve(&self.room_id, &prev_events).await?;

        let new_event = NewEvent {
            event_content: content.into(),
            sender: sender.clone(),
            state_key: state_key.map(String::from),
            redacts: None,
            unsigned: None,
        };

        let auth_events = kerux_core::state::calc_auth_events(&new_event, &state);
        let pdu = VersionedPdu::V4(
            UnhashedPdu {
                event_content: new_event.event_content,
                room_id: self.room_id.clone(),
                sender: new_event.sender,
                state_key: new_event.state_key,
                unsigned: None,
                redacts: None,
                origin: "example.org".parse().unwrap(),
                origin_server_ts: 0,
                prev_events: prev_events.clone(),
                depth: depth as u64,
                auth_events,
            }
            .finalize(),
        );
        let event_id = pdu.event_id();

        assert!(self.depth_map.len() >= depth, "can't insert event there");
        if self.depth_map.len() == depth {
            self.depth_map.push(Vec::new());
        }
        self.depth_map[depth].push(event_id.clone());

        let auth_status = kerux_core::auth::auth_check_v1(self.db, &pdu, &state).await?;
        self.db
            .add_pdus(&[StoredPdu {
                inner: pdu,
                auth_status,
            }])
            .await?;

        Ok(event_id)
    }
}

#[tokio::test]
async fn linear() -> Result<(), Error> {
    let storage_manager = kerux_storage_mem::MemStorageManager::new();
    let db = storage_manager.get_handle().await?;
    let resolver = StateResolver::new(storage_manager.get_handle().await?);

    let alice = MatrixId::new("alice", "example.org".parse().unwrap()).unwrap();
    let room_id = "!linear:example.org".parse().unwrap();
    let mut room = TestRoom::create(&*db, &room_id, &alice).await?;
    let _alice_join = room
        .add(
            1,
            &alice,
            Member {
                avatar_url: None,
                displayname: None,
                membership: Membership::Join,
                is_direct: Some(false),
            },
            Some(&alice.to_string()),
            &resolver,
        )
        .await?;
    let name1 = room
        .add(
            2,
            &alice,
            Name {
                name: Some(String::from("one")),
            },
            Some(""),
            &resolver,
        )
        .await?;

    let state1 = resolver.resolve(&room_id, &[name1.clone()]).await?;
    assert_eq!(
        state1
            .get_content::<Name>(&*db, "")
            .await?
            .unwrap()
            .name
            .as_deref(),
        Some("one")
    );

    let name2 = room
        .add(
            3,
            &alice,
            Name {
                name: Some(String::from("two")),
            },
            Some(""),
            &resolver,
        )
        .await?;
    let state2 = resolver.resolve(&room_id, &[name2]).await?;
    assert_eq!(
        state2
            .get_content::<Name>(&*db, "")
            .await?
            .unwrap()
            .name
            .as_deref(),
        Some("two")
    );
    let state1 = resolver.resolve(&room_id, &[name1]).await?;
    assert_eq!(
        state1
            .get_content::<Name>(&*db, "")
            .await?
            .unwrap()
            .name
            .as_deref(),
        Some("one")
    );
    Ok(())
}
