use std::collections::HashMap;

use serde::Serialize;
use serde_json::Value as JsonValue;

use crate::Cx;

use kerux_extractors::Json;

#[derive(Serialize)]
pub struct ServerKeys {
    server_name: String,
    verify_keys: HashMap<String, VerifyKey>,
    valid_until_ts: u64,
    signatures: HashMap<String, JsonValue>,
    old_verify_keys: HashMap<String, OldVerifyKey>,
}

#[derive(Serialize)]
struct VerifyKey {
    key: String,
}

#[derive(Serialize)]
struct OldVerifyKey {
    key: String,
    expired_ts: u64,
}

pub async fn get_keys(cx: Cx) -> Json<ServerKeys> {
    Json(ServerKeys {
        server_name: cx.domain.to_string(),
        verify_keys: HashMap::new(),
        valid_until_ts: 0,
        signatures: HashMap::new(),
        old_verify_keys: HashMap::new(),
    })
}
