use std::sync::Arc;

use axum::{extract::State, routing::get, Router};
use kerux_extractors::Json;
use serde_json::{json, Value as JsonValue};

use kerux_core::{state::StateResolver, storage::StorageManager, Domain};

pub mod discovery;
mod keys;

type Cx = State<SsApiContext>;
#[derive(Clone)]
pub struct SsApiContext {
    pub domain: Domain,
    pub db_pool: Arc<dyn StorageManager>,
    pub state_resolver: Arc<StateResolver>,
}

pub fn federation_router() -> Router<SsApiContext> {
    let api = Router::new().route("/version", get(server_version));
    Router::new().nest("/v1", api)
}

pub fn keys_router() -> Router<SsApiContext> {
    let api = Router::new().route("/server", get(keys::get_keys));
    Router::new().nest("/v2", api)
}

async fn server_version() -> Json<JsonValue> {
    Json(json!({
        "server": {
            "name": "kerux",
            "version": env!("CARGO_PKG_VERSION")
        }
    }))
}
