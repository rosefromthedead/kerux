use std::{error::Error as StdError, net::SocketAddr, str::FromStr};

use hickory_resolver::error::ResolveErrorKind;
use serde::Deserialize;

pub struct ServerInfo {
    /// The value of the `Host:` header for requests made to the server.
    pub http_host: String,
    /// The server must provide a TLS certificate for this name.
    pub tls_domain: String,
    /// The address to which requests should be made.
    pub address: String,
    /// The port on which the server is listening for requests.
    pub port: u16,
}

/// ref https://spec.matrix.org/v1.8/appendices/#server-name
fn parse_server_name(name: &str) -> Option<(&str, Option<u16>)> {
    if !kerux_core::Domain::is_valid(name) {
        return None;
    }
    match name.rsplit_once(':') {
        // port ends with ']' => "port" is actually the last component of an IPv6 literal
        Some((_host, port)) if port.ends_with(']') => Some((name, None)),
        Some((host, port)) => Some((host, Some(port.parse().ok()?))),
        None => Some((name, None)),
    }
}

#[derive(Deserialize)]
struct WellKnownServerResponse {
    #[serde(rename = "m.server")]
    server: String,
}

const MATRIX_FED_DEFAULT_PORT: u16 = 8448;

/// ref https://spec.matrix.org/v1.8/server-server-api/#server-discovery
///
/// We try to return Err(_) for I/O errors and Ok(None) for absent homeservers/malformed data.
/// In general it's not possible to distinguish these though, since we can't tell between our link
/// being down and the other side's link being down.
/// In federation usage, Err(_) and Ok(None) should be treated the same, but for `kerux resolve` we
/// tell the user our guess at whose fault it is that we didn't find a homeserver.
///
/// `[std::error::Error]` is used instead of `[kerux_core::Error]` because the error shouldn't be
/// conveyed to clients.
#[async_recursion::async_recursion]
async fn resolve_server_name_inner(
    server_name: &str,
    allow_well_known: bool,
) -> Result<Option<ServerInfo>, Box<dyn StdError>> {
    let Some((hostname, port)) = parse_server_name(server_name) else {
        return Ok(None);
    };

    // step 1
    if SocketAddr::from_str(hostname).is_ok() {
        return Ok(Some(ServerInfo {
            http_host: server_name.to_owned(),
            tls_domain: hostname.to_owned(),
            address: hostname.to_owned(),
            port: port.unwrap_or(MATRIX_FED_DEFAULT_PORT),
        }));
    }

    // step 2
    if let Some(port) = port {
        return Ok(Some(ServerInfo {
            http_host: server_name.to_owned(),
            tls_domain: hostname.to_owned(),
            address: hostname.to_owned(),
            port,
        }));
    }

    if allow_well_known {
        // step 3
        // TODO: caching (success and error), exponential backoff
        let resp = reqwest::get(format!("https://{hostname}/.well-known/matrix/server")).await;
        match resp {
            Ok(resp) => {
                let resp: Result<WellKnownServerResponse, _> = resp.json().await;
                if let Ok(resp) = resp {
                    // go round again but without step 3
                    if let Some(resolved) = resolve_server_name_inner(&resp.server, false).await? {
                        return Ok(Some(resolved));
                    }
                }
            }
            Err(e) if e.is_timeout() || e.is_connect() => return Err(e.into()),
            Err(e) => {
                eprintln!("{e}")
            }
        }
    }

    // step 4
    let resolver = hickory_resolver::TokioAsyncResolver::tokio_from_system_conf()?;
    let res = resolver
        .srv_lookup(format!("_matrix-fed._tcp.{hostname}."))
        .await;
    let mut srvs: Vec<_> = match res {
        Ok(v) => v.into_iter().collect(),
        Err(e) if matches!(e.kind(), ResolveErrorKind::NoRecordsFound { .. }) => Vec::new(),
        Err(e) => return Err(e.into()),
    };
    // crude as hell. i yearn for a crate that does srv logic
    srvs.sort_by_key(|srv| (srv.priority() as u32) << 16 | (u16::MAX - srv.weight()) as u32);
    if let Some(srv) = srvs.first() {
        return Ok(Some(ServerInfo {
            http_host: hostname.to_owned(),
            tls_domain: hostname.to_owned(),
            address: srv.target().to_utf8(),
            port: srv.port(),
        }));
    }

    // step 5 is deprecated

    // step 6
    return Ok(Some(ServerInfo {
        http_host: hostname.to_owned(),
        tls_domain: hostname.to_owned(),
        address: hostname.to_owned(),
        port: MATRIX_FED_DEFAULT_PORT,
    }));
}

pub async fn resolve_server_name(
    server_name: &str,
) -> Result<Option<ServerInfo>, Box<dyn StdError>> {
    resolve_server_name_inner(server_name, true).await
}

#[cfg(test)]
mod tests {
    #[tokio::test]
    async fn matrix_org() {
        super::resolve_server_name("matrix.org").await.unwrap();
    }
}
