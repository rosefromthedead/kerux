use axum::{
    extract::{rejection::JsonRejection, FromRequest, FromRequestParts, Request},
    http::request::Parts,
    response::IntoResponse,
};
use serde::{de::DeserializeOwned, Serialize};

use kerux_core::ErrorKind;

pub struct Json<T>(pub T);
#[async_trait::async_trait]
impl<T, S> FromRequest<S> for Json<T>
where
    T: DeserializeOwned,
    S: Send + Sync,
{
    type Rejection = kerux_core::Error;
    async fn from_request(req: Request, s: &S) -> Result<Self, kerux_core::Error> {
        let err = match axum::Json::from_request(req, s).await {
            Ok(axum::Json(v)) => return Ok(Json(v)),
            Err(e) => e,
        };
        Err(match err {
            JsonRejection::JsonDataError(e) => ErrorKind::BadJson.with(e),
            JsonRejection::JsonSyntaxError(e) => ErrorKind::NotJson.with(e),
            JsonRejection::MissingJsonContentType(e) => ErrorKind::NotJson.with(e),
            _ => ErrorKind::Unknown.with(err),
        })
    }
}
impl<T: Serialize> IntoResponse for Json<T> {
    fn into_response(self) -> axum::response::Response {
        axum::Json(self.0).into_response()
    }
}

pub struct Path<T>(pub T);
#[async_trait::async_trait]
impl<T, S> FromRequestParts<S> for Path<T>
where
    T: DeserializeOwned + Send,
    S: Send + Sync,
{
    type Rejection = kerux_core::Error;
    async fn from_request_parts(req: &mut Parts, s: &S) -> Result<Self, kerux_core::Error> {
        match axum::extract::Path::from_request_parts(req, s).await {
            Ok(axum::extract::Path(v)) => return Ok(Path(v)),
            Err(e) => return Err(ErrorKind::NotFound.with(e)),
        }
    }
}

pub struct Query<T>(pub T);
#[async_trait::async_trait]
impl<T, S> FromRequestParts<S> for Query<T>
where
    T: DeserializeOwned + Default,
    S: Send + Sync,
{
    type Rejection = kerux_core::Error;
    async fn from_request_parts(req: &mut Parts, s: &S) -> Result<Self, kerux_core::Error> {
        if req.uri.query().is_none() {
            return Ok(Query(T::default()));
        }
        match axum::extract::Query::from_request_parts(req, s).await {
            Ok(axum::extract::Query(v)) => Ok(Query(v)),
            // serde_urlencoded doesn't seem to give us anything to work with
            Err(e) => Err(ErrorKind::Unknown.with(e)),
        }
    }
}
