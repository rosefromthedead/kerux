use axum::{
    http::{HeaderName, Method},
    Router,
};
use fs_err::tokio::read_to_string;
use serde::Deserialize;
use std::{net::SocketAddr, process::ExitCode, sync::Arc};
use tower_http::{cors, trace::TraceLayer};
use tracing_subscriber::EnvFilter;

use kerux_core::{state::StateResolver, storage::StorageManager, Domain};

#[derive(Deserialize, Clone, Copy, PartialEq, Eq)]
pub enum DatabaseType {
    #[cfg(feature = "storage-mem")]
    #[serde(rename = "mem")]
    InMemory,
    #[cfg(feature = "storage-sled")]
    #[serde(rename = "sled")]
    Sled,
}
#[derive(Deserialize)]
pub struct Config {
    domain: Domain,
    bind_address: SocketAddr,
    storage: DatabaseType,
}

pub struct ServerState {
    pub config: Config,
    pub db_pool: Arc<dyn StorageManager>,
    pub state_resolver: Arc<StateResolver>,
}

#[tokio::main]
async fn main() -> ExitCode {
    tracing_subscriber::fmt()
        .pretty()
        .with_env_filter(EnvFilter::from_default_env())
        .init();

    let mut args = std::env::args();
    let command = args.nth(1);
    if command.as_deref() == Some("resolve") {
        let Some(name) = args.next() else {
            eprintln!("missing argument: server name");
            return ExitCode::FAILURE;
        };
        let result = match kerux_server_api::discovery::resolve_server_name(&name).await {
            Ok(v) => v,
            Err(e) => {
                eprintln!("{e}");
                return ExitCode::FAILURE;
            }
        };
        if let Some(result) = result {
            println!("HTTP Host: {}", result.http_host);
            println!("TLS Domain: {}", result.tls_domain);
            println!("Address: {}", result.address);
            println!("Port: {}", result.port);
        } else {
            println!("No valid federating Matrix server found with this name");
        }
        return ExitCode::SUCCESS;
    } else if command.is_some() {
        eprintln!("unrecognised command");
        return ExitCode::FAILURE;
    }

    if let Err(e) = run().await {
        eprintln!("{e}");
        return ExitCode::FAILURE;
    }

    ExitCode::SUCCESS
}

async fn run() -> Result<(), Box<dyn std::error::Error>> {
    let config: Config = toml::from_str(&read_to_string("config.toml").await?)?;
    let db_pool: Arc<dyn StorageManager> = match config.storage {
        #[cfg(feature = "storage-mem")]
        DatabaseType::InMemory => Arc::new(kerux_storage_mem::MemStorageManager::new()) as _,
        #[cfg(feature = "storage-sled")]
        DatabaseType::Sled => Arc::new(kerux_storage_sled::SledStorage::new("sled")?) as _,
    };
    let state_resolver = Arc::new(StateResolver::new(db_pool.get_handle().await?));

    let cs_api_context = kerux_client_api::CsApiContext {
        domain: config.domain.clone(),
        db_pool: Arc::clone(&db_pool),
        state_resolver: Arc::clone(&state_resolver),
    };

    let ss_api_context = kerux_server_api::SsApiContext {
        domain: config.domain.clone(),
        db_pool: Arc::clone(&db_pool),
        state_resolver: Arc::clone(&state_resolver),
    };

    let router: Router<()> = axum::Router::new()
        .nest(
            "/_matrix/client",
            kerux_client_api::router().with_state(cs_api_context).layer(
                // ref https://spec.matrix.org/v1.8/client-server-api/#web-browser-clients
                tower_http::cors::CorsLayer::new()
                    .allow_origin(cors::Any)
                    .allow_methods([
                        Method::GET,
                        Method::POST,
                        Method::PUT,
                        Method::DELETE,
                        Method::OPTIONS,
                    ])
                    .allow_headers(
                        ["x-requested-with", "content-type", "authorization"]
                            .map(HeaderName::from_static),
                    ),
            ),
        )
        .nest(
            "/_matrix/federation",
            kerux_server_api::federation_router().with_state(ss_api_context.clone()),
        )
        .nest(
            "/_matrix/keys",
            kerux_server_api::keys_router().with_state(ss_api_context),
        )
        .layer(TraceLayer::new_for_http());
    let listener = tokio::net::TcpListener::bind(&config.bind_address).await?;
    axum::serve(listener, router.into_make_service()).await?;

    Ok(())
}
