use std::{
    collections::HashMap,
    sync::{Arc, Mutex},
    time::{Duration, Instant},
};

use async_trait::async_trait;
use itertools::Itertools;
use serde::{de::DeserializeOwned, Deserialize, Serialize};
use serde_json::Value as JsonValue;
use sled::{
    transaction::{ConflictableTransactionError, TransactionalTree},
    Db, IVec, Tree,
};
use uuid::Uuid;

use kerux_core::{
    error::{Error, ErrorKind, ResultExt},
    events::{ephemeral::Typing, pdu::StoredPdu},
    storage::{AccessTokenData, Batch, EventQuery, Storage, StorageManager, UserProfile},
    MatrixId, RoomId,
};

trait TreeExt {
    type Error;
    fn get_value<K: AsRef<[u8]>, V: DeserializeOwned>(
        &self,
        key: K,
    ) -> Result<Option<V>, Self::Error>;
    /// Returns whether the insert succeeded (i.e. the key was not already present)
    fn try_insert_value<K: AsRef<[u8]>, V: Serialize>(
        &self,
        key: K,
        value: V,
    ) -> Result<bool, Self::Error>;
    /// Returns whether something was overwritten (i.e. the key was already present)
    fn overwrite_value<K: AsRef<[u8]>, V: Serialize>(
        &self,
        key: K,
        value: V,
    ) -> Result<bool, Self::Error>;
}

impl TreeExt for Tree {
    type Error = Error;
    fn get_value<K: AsRef<[u8]>, V: DeserializeOwned>(&self, key: K) -> Result<Option<V>, Error> {
        self.get(key)
            .with(ErrorKind::Unknown)?
            .map(|bytes| deserialize(&bytes))
            .transpose()
            .map_err(Into::into)
    }

    fn try_insert_value<K: AsRef<[u8]>, V: Serialize>(
        &self,
        key: K,
        value: V,
    ) -> Result<bool, Error> {
        let bytes = serialize(&value).with(ErrorKind::Unknown)?;
        if self.contains_key(&key).with(ErrorKind::Unknown)? {
            return Ok(false);
        }
        self.insert(key, &*bytes).with(ErrorKind::Unknown)?;
        Ok(true)
    }

    fn overwrite_value<K: AsRef<[u8]>, V: Serialize>(
        &self,
        key: K,
        value: V,
    ) -> Result<bool, Error> {
        let bytes = serialize(&value)?;
        let was_there = self
            .insert(key, &*bytes)
            .with(ErrorKind::Unknown)?
            .is_some();
        Ok(was_there)
    }
}

impl TreeExt for TransactionalTree {
    type Error = ConflictableTransactionError<Error>;
    fn get_value<K: AsRef<[u8]>, V: DeserializeOwned>(
        &self,
        key: K,
    ) -> Result<Option<V>, Self::Error> {
        self.get(key)?
            .map(|bytes| deserialize(&bytes))
            .transpose()
            .map_err(ConflictableTransactionError::Abort)
    }

    fn try_insert_value<K: AsRef<[u8]>, V: Serialize>(
        &self,
        key: K,
        value: V,
    ) -> Result<bool, Self::Error> {
        let bytes = serialize(&value).map_err(ConflictableTransactionError::Abort)?;
        if self.get(&key)?.is_some() {
            return Ok(false);
        }
        self.insert(IVec::from(key.as_ref()), &*bytes)?;
        Ok(true)
    }

    fn overwrite_value<K: AsRef<[u8]>, V: Serialize>(
        &self,
        key: K,
        value: V,
    ) -> Result<bool, Self::Error> {
        let bytes = serialize(&value).map_err(ConflictableTransactionError::Abort)?;
        let was_there = self.insert(IVec::from(key.as_ref()), &*bytes)?.is_some();
        Ok(was_there)
    }
}

#[derive(Default, Deserialize, Serialize)]
struct User {
    password_hash: String,
    profile: UserProfile,
    account_data: HashMap<String, JsonValue>,
}

#[derive(Default)]
struct Ephemeral {
    ephemeral: HashMap<String, JsonValue>,
    typing: HashMap<MatrixId, Instant>,
}

impl Ephemeral {
    fn get_typing(&self) -> Typing {
        let now = Instant::now();
        let mut ret = Typing::default();
        for (mxid, _) in self.typing.iter().filter(|(_, timeout)| **timeout > now) {
            ret.user_ids.insert(mxid.clone());
        }
        ret
    }
}

pub struct SledStorage(SledStorageHandle);

impl SledStorage {
    pub fn new(path: &str) -> Result<Self, Error> {
        let db = sled::open(path).with(ErrorKind::Unknown)?;
        Ok(Self(SledStorageHandle {
            all: db.clone(),
            events: db.open_tree("events").with(ErrorKind::Unknown)?,
            rooms: db.open_tree("rooms").with(ErrorKind::Unknown)?,
            users: db.open_tree("users").with(ErrorKind::Unknown)?,
            access_tokens: db.open_tree("access_tokens").with(ErrorKind::Unknown)?,
            txn_ids: db.open_tree("txn_ids").with(ErrorKind::Unknown)?,
            batches: db.open_tree("batches").with(ErrorKind::Unknown)?,
            room_orderings: Arc::new(Mutex::new(HashMap::new())),
            headless_events: db.open_tree("headless_events").with(ErrorKind::Unknown)?,
            ephemeral: Arc::new(Mutex::new(HashMap::new())),
        }))
    }
}

#[async_trait]
impl StorageManager for SledStorage {
    async fn get_handle(&self) -> Result<Box<dyn Storage>, Error> {
        Ok(Box::new(self.0.clone()))
    }
}

#[derive(Clone)]
pub struct SledStorageHandle {
    all: Db,
    events: Tree,
    rooms: Tree,
    users: Tree,
    access_tokens: Tree,
    txn_ids: Tree,
    batches: Tree,
    /// Map of room ids -> event orderings
    room_orderings: Arc<Mutex<HashMap<RoomId, Tree>>>,
    headless_events: Tree,
    ephemeral: Arc<Mutex<HashMap<RoomId, Ephemeral>>>,
}

impl SledStorageHandle {
    async fn get_room_ordering_tree(&self, room_id: &RoomId) -> Result<Tree, Error> {
        let mut ordering_trees = self
            .room_orderings
            .lock()
            .map_err(|_| ErrorKind::Unknown.message("a database lock was poisoned!"))?;
        if let Some(tree) = ordering_trees.get(room_id) {
            Ok(tree.clone())
        } else {
            let tree = self
                .all
                .open_tree(room_id.to_string())
                .with(ErrorKind::Unknown)?;
            ordering_trees.insert(room_id.to_owned(), tree.clone());
            Ok(tree)
        }
    }

    async fn get_events(
        &self,
        ordering_tree: &Tree,
        query: &EventQuery<'_>,
        from: usize,
        to: Option<usize>,
    ) -> Result<(Vec<StoredPdu>, usize), Error> {
        let mut ret = Vec::new();

        let from_bytes = from.to_be_bytes();
        let to_bytes = to.map(usize::to_be_bytes);
        let pdu_iter = match to_bytes {
            Some(to_bytes) => ordering_tree.range(from_bytes..=to_bytes),
            None => ordering_tree.range(from_bytes..),
        }
        .map_ok(|(_key, event_id)| {
            self.events.get(&format!(
                "{}_{}",
                query.room_id,
                String::from_utf8(Vec::from(event_id.as_ref())).unwrap()
            ))
        })
        // flatten
        .map(|res| match res {
            Ok(Ok(v)) => Ok(v),
            Ok(Err(e)) | Err(e) => Err(e),
        });
        let mut end = 0;
        for pdu in pdu_iter {
            // is Ok(None) if the event is not present, but it must be present if it's in the
            // ordering tree
            let pdu = pdu.with(ErrorKind::Unknown)?;
            let pdu: StoredPdu = deserialize(pdu.unwrap().as_ref())?;
            if query.matches(&pdu.inner()) {
                ret.push(pdu);
            }
            end += 1;
        }
        Ok((ret, end + from))
    }
}
fn deserialize<'a, T: Deserialize<'a>>(bytes: &'a [u8]) -> Result<T, Error> {
    serde_json::from_slice(bytes).with(ErrorKind::Unknown)
}
fn serialize<T: Serialize>(value: &T) -> Result<Vec<u8>, Error> {
    serde_json::to_vec(value).with(ErrorKind::Unknown)
}

#[async_trait]
impl Storage for SledStorageHandle {
    async fn create_user(&self, username: &str, password: &str) -> Result<(), Error> {
        let salt: [u8; 16] = rand::random();
        let password_hash = argon2::hash_encoded(password.as_bytes(), &salt, &Default::default())
            .with(ErrorKind::Unknown)?;
        let did_insert = self.users.try_insert_value(
            username,
            &User {
                password_hash: password_hash.to_string(),
                ..Default::default()
            },
        )?;
        match did_insert {
            true => Ok(()),
            false => Err(ErrorKind::UsernameTaken.into()),
        }
    }

    async fn verify_password(&self, username: &str, password: &str) -> Result<bool, Error> {
        let user: Option<User> = self.users.get_value(username)?;
        if let Some(user) = user {
            match argon2::verify_encoded(&user.password_hash, password.as_bytes()) {
                Ok(true) => Ok(true),
                Ok(false) => Ok(false),
                Err(_) => Ok(false),
            }
        } else {
            Ok(false)
        }
    }

    async fn create_access_token(&self, username: &str, device_id: &str) -> Result<Uuid, Error> {
        let token = Uuid::new_v4();
        if !self.users.contains_key(username).with(ErrorKind::Unknown)? {
            return Err(ErrorKind::UserNotFound.into());
        }
        self.access_tokens.try_insert_value(
            token.as_bytes(),
            &AccessTokenData {
                username: username.to_string(),
                device_id: device_id.to_string(),
            },
        )?;
        Ok(token)
    }

    async fn delete_access_token(&self, token: Uuid) -> Result<(), Error> {
        self.access_tokens
            .remove(token.as_bytes())
            .with(ErrorKind::UnknownToken)?;
        Ok(())
    }

    async fn delete_all_access_tokens(&self, token: Uuid) -> Result<(), Error> {
        let data: Option<AccessTokenData> = self.access_tokens.get_value(token.as_bytes())?;
        if let Some(data) = data {
            let username = data.username;
            let iter = (&self.access_tokens).into_iter();
            let mut to_delete = Vec::new();
            for res in iter {
                let (key, val) = res.with(ErrorKind::Unknown)?;
                let data = deserialize::<AccessTokenData>(&val).unwrap();
                if data.username == username {
                    to_delete.push(key);
                }
            }
            for key in to_delete.into_iter() {
                self.access_tokens.remove(key).with(ErrorKind::Unknown)?;
            }
        }
        Ok(())
    }

    async fn try_auth(&self, token: Uuid) -> Result<Option<AccessTokenData>, Error> {
        let maybe_token_data = self.access_tokens.get_value(token.as_bytes())?;
        Ok(maybe_token_data)
    }

    async fn record_txn(&self, token: Uuid, txn_id: String) -> Result<bool, Error> {
        let name = format!("{}_{}", token, txn_id);
        let success = self.txn_ids.try_insert_value(&name, ())?;
        Ok(success)
    }

    async fn get_profile(&self, username: &str) -> Result<UserProfile, Error> {
        self.users
            .get_value(username)?
            .ok_or_else(|| Error::from(ErrorKind::UserNotFound))
    }

    async fn set_profile(&self, username: &str, profile: UserProfile) -> Result<(), Error> {
        let mut user: User = self
            .users
            .get_value(username)?
            .ok_or(ErrorKind::UserNotFound)?;
        user.profile = profile;
        self.users.overwrite_value(username, user)?;
        Ok(())
    }

    async fn add_pdus(&self, pdus: &[StoredPdu]) -> Result<(), Error> {
        for pdu in pdus {
            let name = format!("{}_{}", pdu.room_id(), pdu.event_id());
            self.events.try_insert_value(name, pdu)?;
            let ordering_tree = self.get_room_ordering_tree(&pdu.room_id()).await?;
            'cas: loop {
                let idx = match ordering_tree.last().with(ErrorKind::Unknown)? {
                    Some((k, _v)) => u32::from_be_bytes(k[0..4].try_into().unwrap()) + 1,
                    None => 0,
                };
                let res = ordering_tree
                    .compare_and_swap(
                        &u32::to_be_bytes(idx),
                        Option::<&[u8]>::None,
                        Some(&*pdu.event_id()),
                    )
                    .with(ErrorKind::Unknown)?;
                if res.is_ok() {
                    break 'cas;
                }
            }
            for prev_event in pdu.prev_events() {
                self.headless_events
                    .remove(&format!("{}~{}", pdu.room_id(), prev_event))
                    .with(ErrorKind::Unknown)?;
            }
            self.headless_events
                .insert(&format!("{}~{}", pdu.room_id(), pdu.event_id()), &[])
                .with(ErrorKind::Unknown)?;
            self.rooms
                .insert(pdu.room_id().to_string(), &[])
                .with(ErrorKind::Unknown)?;
        }
        Ok(())
    }

    async fn get_prev_events(&self, room_id: &RoomId) -> Result<(Vec<String>, u64), Error> {
        let max_depth: u64 = match self.headless_events.get_value(room_id.to_string())? {
            Some(v) => v,
            None => {
                tracing::warn!(?room_id, "room is half-created!");
                0
            }
        };
        let mut prefix = room_id.to_string().into_bytes();
        prefix.push(b'~');
        self.headless_events
            .scan_prefix(&prefix)
            .keys()
            .map_ok(|k| k.split(|&b| b == b'~').nth(1).unwrap().to_owned())
            .map_ok(String::from_utf8)
            .map_ok(Result::unwrap)
            .collect::<Result<Vec<String>, sled::Error>>()
            .with(ErrorKind::Unknown)
            .map(|prev_events| (prev_events, max_depth))
    }
    async fn get_rooms(&self) -> Result<Vec<RoomId>, Error> {
        self.rooms
            .iter()
            .map_ok(|(key, _value)| {
                String::from_utf8(Vec::from(key.as_ref()))
                    .unwrap()
                    .parse()
                    .unwrap()
            })
            .collect::<Result<Vec<_>, _>>()
            .with(ErrorKind::Unknown)
    }

    async fn query_pdus<'a>(
        &self,
        query: EventQuery<'a>,
        wait: bool,
    ) -> Result<(Vec<StoredPdu>, usize), Error> {
        let ordering_tree = self.get_room_ordering_tree(&query.room_id).await?;
        if ordering_tree.is_empty() {
            return Err(ErrorKind::RoomNotFound.into());
        }

        let (mut from, mut to) = (query.from, query.to);

        let res = self.get_events(&ordering_tree, &query, from, to).await?;

        // if we don't need to wait, return asap
        if !(wait && res.0.is_empty()) {
            return Ok(res);
        }

        self.events.watch_prefix(query.room_id.to_string()).await;
        from = to.unwrap();
        to = None;

        // this time we roll with it
        self.get_events(&ordering_tree, &query, from, to).await
    }

    async fn get_pdu(&self, room_id: &RoomId, event_id: &str) -> Result<Option<StoredPdu>, Error> {
        self.events
            .get_value(&format!("{}_{}", room_id, event_id))
            .map_err(Into::into)
    }

    async fn get_all_ephemeral(
        &self,
        room_id: &RoomId,
    ) -> Result<HashMap<String, JsonValue>, Error> {
        //TODO: this inserts an ephemeral entry even if the room doesn't actually exist - figure
        // out what to do about it
        let mut ephemerals = self
            .ephemeral
            .lock()
            .map_err(|_| ErrorKind::Unknown.message("a database lock was poisoned!"))?;
        let ephemeral = ephemerals.entry(room_id.to_owned()).or_default();
        let mut ret = ephemeral.ephemeral.clone();
        ret.insert(
            String::from("m.typing"),
            serde_json::to_value(ephemeral.get_typing()).unwrap(),
        );
        Ok(ret)
    }

    async fn get_ephemeral(
        &self,
        room_id: &RoomId,
        event_type: &str,
    ) -> Result<Option<JsonValue>, Error> {
        let mut ephemerals = self
            .ephemeral
            .lock()
            .map_err(|_| ErrorKind::Unknown.message("a database lock was poisoned!"))?;
        let ephemeral = ephemerals.entry(room_id.to_owned()).or_default();
        if event_type == "m.typing" {
            let typing = ephemeral.get_typing();
            match typing.user_ids.is_empty() {
                true => Ok(None),
                false => Ok(Some(serde_json::to_value(typing).unwrap())),
            }
        } else {
            Ok(ephemeral.ephemeral.get(event_type).cloned())
        }
    }

    async fn set_ephemeral(
        &self,
        room_id: &RoomId,
        event_type: &str,
        content: Option<JsonValue>,
    ) -> Result<(), Error> {
        assert!(
            event_type != "m.typing",
            "m.typing should not be set directly"
        );
        let mut ephemerals = self
            .ephemeral
            .lock()
            .map_err(|_| ErrorKind::Unknown.message("a database lock was poisoned!"))?;
        let ephemeral = ephemerals.entry(room_id.to_owned()).or_default();
        match content {
            Some(c) => ephemeral.ephemeral.insert(String::from(event_type), c),
            None => ephemeral.ephemeral.remove(event_type),
        };
        Ok(())
    }

    async fn set_typing(
        &self,
        room_id: &RoomId,
        user_id: &MatrixId,
        is_typing: bool,
        timeout: u32,
    ) -> Result<(), Error> {
        let mut ephemerals = self
            .ephemeral
            .lock()
            .map_err(|_| ErrorKind::Unknown.message("a database lock was poisoned!"))?;
        let ephemeral = ephemerals.entry(room_id.to_owned()).or_default();
        if is_typing {
            ephemeral.typing.insert(
                user_id.clone(),
                Instant::now() + Duration::from_millis(timeout as u64),
            );
        } else {
            ephemeral.typing.remove(user_id);
        }

        Ok(())
    }

    async fn get_user_account_data(
        &self,
        username: &str,
    ) -> Result<HashMap<String, JsonValue>, Error> {
        let user: User = self
            .users
            .get_value(username)?
            .ok_or(ErrorKind::UserNotFound)?;
        Ok(user.account_data.clone())
    }

    async fn get_batch(&self, id: &str) -> Result<Option<Batch>, Error> {
        self.batches.get_value(id)
    }

    async fn set_batch(&self, id: &str, batch: Batch) -> Result<(), Error> {
        self.batches.overwrite_value(id, batch).map(drop)
    }
}
