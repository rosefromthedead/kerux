use serde::{Deserialize, Serialize};
use serde_json::{json, Value as JsonValue};
use tracing::instrument;

use kerux_core::{storage::UserProfile, Error, ErrorKind, MatrixId};
use kerux_extractors::{Json, Path};

use crate::{auth::AuthenticatedUser, Cx};

// GET /profile/:user_id/avatar_url
#[instrument(skip(cx), err)]
pub async fn get_avatar_url(
    cx: Cx,
    Path(user_id): Path<MatrixId>,
) -> Result<Json<JsonValue>, Error> {
    if user_id.domain() != &cx.domain {
        return Err(ErrorKind::Unimplemented.into());
    }

    let db = cx.db_pool.get_handle().await?;
    let avatar_url = match db.get_profile(&user_id.localpart()).await?.avatar_url {
        Some(v) => v,
        None => return Err(ErrorKind::NotFound.into()),
    };

    Ok(Json(json!({ "avatar_url": avatar_url })))
}

#[derive(Debug, Deserialize)]
pub struct SetAvatarUrlRequest {
    avatar_url: String,
}

// PUT /profile/:user_id/avatar_url
#[instrument(skip(cx), err)]
pub async fn set_avatar_url(
    cx: Cx,
    authed_user: AuthenticatedUser,
    Path(target_user): Path<MatrixId>,
    Json(req): Json<SetAvatarUrlRequest>,
) -> Result<Json<()>, Error> {
    let db = cx.db_pool.get_handle().await?;

    if target_user != authed_user.id {
        return Err(ErrorKind::Forbidden.into());
    }
    if target_user.domain() != &cx.domain {
        return Err(ErrorKind::UserNotFound
            .message("User does not live on this homeserver")
            .into());
    }

    db.set_avatar_url(&authed_user.id.localpart(), &req.avatar_url)
        .await?;
    Ok(Json(()))
}

// GET /profile/:user_id/displayname
#[instrument(skip(cx), err)]
pub async fn get_display_name(
    cx: Cx,
    Path(user_id): Path<MatrixId>,
) -> Result<Json<JsonValue>, Error> {
    if user_id.domain() != &cx.domain {
        return Err(ErrorKind::UserNotFound
            .message("User does not live on this homeserver")
            .into());
    }

    let db = cx.db_pool.get_handle().await?;
    let displayname = match db.get_profile(&user_id.localpart()).await?.displayname {
        Some(v) => v,
        None => return Err(ErrorKind::NotFound.into()),
    };

    Ok(Json(json!({ "displayname": displayname })))
}

#[derive(Debug, Deserialize)]
pub struct SetDisplayNameRequest {
    displayname: String,
}

// PUT /profile/:user_id/displayname
#[instrument(skip(cx), err)]
pub async fn set_display_name(
    cx: Cx,
    authed_user: AuthenticatedUser,
    Path(target_user): Path<MatrixId>,
    Json(req): Json<SetDisplayNameRequest>,
) -> Result<Json<()>, Error> {
    let db = cx.db_pool.get_handle().await?;

    if target_user != authed_user.id {
        return Err(ErrorKind::Forbidden.into());
    }
    if target_user.domain() != &cx.domain {
        return Err(ErrorKind::UserNotFound
            .message("User does not live on this homeserver")
            .into());
    }

    db.set_display_name(&authed_user.id.localpart(), &req.displayname)
        .await?;
    Ok(Json(()))
}

// GET /profile/:user_id
#[instrument(skip(cx), err)]
pub async fn get_profile(cx: Cx, Path(user_id): Path<MatrixId>) -> Result<Json<JsonValue>, Error> {
    if user_id.domain() != &cx.domain {
        return Err(ErrorKind::UserNotFound
            .message("User does not live on this homeserver")
            .into());
    }

    let db = cx.db_pool.get_handle().await?;
    let UserProfile {
        avatar_url,
        displayname,
    } = db.get_profile(&user_id.localpart()).await?;
    let mut response = serde_json::Map::new();
    if let Some(v) = avatar_url {
        response.insert("avatar_url".into(), v.into());
    }
    if let Some(v) = displayname {
        response.insert("displayname".into(), v.into());
    }

    Ok(Json(response.into()))
}

#[derive(Deserialize)]
pub struct UserDirSearchRequest {
    search_term: String,
}

#[derive(Serialize)]
pub struct UserDirSearchResponse {
    results: Vec<User>,
    limited: bool,
}

#[derive(Serialize)]
struct User {
    user_id: MatrixId,
    #[serde(skip_serializing_if = "Option::is_none")]
    avatar_url: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    display_name: Option<String>,
}

//TODO: actually implement this
// POST /user_directory/search
#[instrument(skip_all, err)]
pub async fn search_user_directory(
    cx: Cx,
    Json(req): Json<UserDirSearchRequest>,
) -> Result<Json<UserDirSearchResponse>, Error> {
    let db = cx.db_pool.get_handle().await?;
    let searched_user = MatrixId::new(&req.search_term, cx.domain.clone())
        .map_err(|e| ErrorKind::Unknown.with(e))?;
    let user_profile = db.get_profile(searched_user.localpart()).await;
    match user_profile {
        Ok(p) => Ok(Json(UserDirSearchResponse {
            results: vec![User {
                user_id: searched_user,
                avatar_url: p.avatar_url,
                display_name: p.displayname,
            }],
            limited: false,
        })),
        Err(e) if matches!(e.kind(), ErrorKind::UserNotFound) => Ok(Json(UserDirSearchResponse {
            results: Vec::new(),
            limited: false,
        })),
        Err(e) => return Err(e),
    }
}

#[derive(Serialize)]
pub struct Get3pidsResponse {
    threepids: Vec<Threepid>,
}

#[derive(Serialize)]
struct Threepid {
    address: String,
    validated_at: u64,
    added_at: u64,
}

// GET /account/3pid
#[instrument(skip_all, err)]
pub async fn get_3pids(_cx: Cx, _user: AuthenticatedUser) -> Result<Json<Get3pidsResponse>, Error> {
    //TODO: implement
    Ok(Json(Get3pidsResponse {
        threepids: Vec::new(),
    }))
}

#[derive(Serialize, Debug)]
pub struct FilterEventsResponse {
    filter_id: String,
}

/// https://spec.matrix.org/v1.7/client-server-api/#post_matrixclientv3useruseridfilter
// POST /user/:user_id/filter
pub async fn filter_events() -> Result<Json<FilterEventsResponse>, Error> {
    // TODO: This should actually be implemented
    Ok(Json(FilterEventsResponse {
        filter_id: "todo".to_owned(),
    }))
}
