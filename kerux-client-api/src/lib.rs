use std::sync::Arc;

use axum::{
    routing::{get, post, put},
    Router,
};
use serde_json::json;

use kerux_core::{state::StateResolver, storage::StorageManager, Domain};
use kerux_extractors::Json;

mod auth;
mod ephemeral;
mod keys;
mod pushrules;
mod room;
mod room_events;
mod user;

type Cx = axum::extract::State<CsApiContext>;
#[derive(Clone)]
pub struct CsApiContext {
    pub domain: Domain,
    pub db_pool: Arc<dyn StorageManager>,
    pub state_resolver: Arc<StateResolver>,
}

pub fn router() -> Router<CsApiContext> {
    let api = Router::<CsApiContext>::new()
        .route(
            "/login",
            get(auth::get_supported_login_types).post(auth::login),
        )
        .route("/logout", post(auth::logout))
        .route("/logout/all", post(auth::logout_all))
        .route("/register", post(auth::register))
        .route("/account/whoami", get(auth::whoami))
        .route(
            "/profile/:user_id/avatar_url",
            get(user::get_avatar_url).put(user::set_avatar_url),
        )
        .route(
            "/profile/:user_id/displayname",
            get(user::get_display_name).put(user::set_display_name),
        )
        .route("/user_directory/search", post(user::search_user_directory))
        .route("/account/3pid", get(user::get_3pids))
        .route("/user/:user_id/filter", post(user::filter_events))
        .route("/createRoom", post(room::create_room))
        .route("/rooms/:room_id/invite", post(room::invite))
        .route("/join/:room_id_or_alias", post(room::join_by_id_or_alias))
        .route("/rooms/:room_id/messages", get(room_events::messages))
        .route("/sync", get(room_events::sync))
        .route(
            "/rooms/:room_id/event/:event_id",
            get(room_events::get_event),
        )
        .route(
            "/rooms/:room_id/state/:event_type",
            get(room_events::get_state_event_no_key),
        )
        .route(
            "/rooms/:room_id/state/:event_type/:state_key",
            get(room_events::get_state_event_key).put(room_events::send_state_event),
        )
        .route("/rooms/:room_id/state", get(room_events::get_state))
        .route("/rooms/:room_id/members", get(room_events::get_members))
        .route(
            "/rooms/:room_id/send/:event_type/:txn_id",
            put(room_events::send_event),
        )
        .route("/rooms/:room_id/typing/:user_id", put(ephemeral::typing))
        .route("/thirdparty/protocols", get(thirdparty_protocols))
        .route("/keys/query", post(keys::query))
        .route("/pushrules", get(pushrules::global));

    Router::new()
        .nest("/r0", api.clone())
        .nest("/v3", api)
        .route("/versions", get(versions))
}

async fn versions() -> Json<serde_json::Value> {
    Json(json!({
        "versions": [
            "v1.7",
        ]
    }))
}

// GET /thirdparty/protocols
async fn thirdparty_protocols() -> Json<serde_json::Value> {
    Json(json!({}))
}
