use axum::{
    extract::FromRequestParts,
    http::{request::Parts, StatusCode},
};
use serde::{Deserialize, Serialize};
use serde_json::json;
use tracing::{field::Empty, instrument, span::Span};
use uuid::Uuid;

use kerux_core::{
    error::{Error, ErrorKind, ResultExt},
    Domain, MatrixId,
};
use kerux_extractors::{Json, Query};

use crate::Cx;

#[derive(Debug, Deserialize)]
enum LoginType {
    #[serde(rename = "m.login.password")]
    Password,
}

pub struct AuthenticatedUser {
    pub id: MatrixId,
    pub device_id: String,
    pub token: Uuid,
}

impl std::fmt::Debug for AuthenticatedUser {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        // allows us to use tracing::instrument on functions that take this, and have logs that look
        // nice and don't contain live tokens
        self.id.localpart().fmt(f)
    }
}

fn get_access_token(req: &Parts) -> Result<Uuid, Error> {
    if let Some(s) = req.headers.get("Authorization") {
        let s: &str = s.to_str().map_err(|_| ErrorKind::MissingToken)?;
        if !s.starts_with("Bearer ") {
            return Err(ErrorKind::MissingToken.into());
        }
        let token = s
            .trim_start_matches("Bearer ")
            .parse::<Uuid>()
            .map_err(|_| ErrorKind::UnknownToken)?;
        Ok(token)
    } else if let Some(pair) = req
        .uri
        .query()
        .ok_or(ErrorKind::MissingToken)?
        .split('&')
        .find(|pair| pair.starts_with("access_token"))
    {
        let token = pair
            .trim_start_matches("access_token=")
            .parse::<Uuid>()
            .map_err(|_| ErrorKind::UnknownToken)?;
        Ok(token)
    } else {
        Err(ErrorKind::MissingToken.into())
    }
}

#[async_trait::async_trait]
impl FromRequestParts<crate::CsApiContext> for AuthenticatedUser {
    type Rejection = Error;
    async fn from_request_parts(req: &mut Parts, cx: &crate::CsApiContext) -> Result<Self, Error> {
        let token = get_access_token(req)?;
        let db = cx.db_pool.get_handle().await?;
        let token_data = db.try_auth(token).await?.ok_or(ErrorKind::UnknownToken)?;
        let id = MatrixId::new(&token_data.username, cx.domain.clone()).unwrap();
        Ok(AuthenticatedUser {
            id,
            device_id: token_data.device_id,
            token,
        })
    }
}

// GET /login
#[instrument]
pub async fn get_supported_login_types() -> Json<serde_json::Value> {
    //TODO: allow config
    Json(json!({
        "flows": [
            {
                "type": "m.login.password"
            }
        ]
    }))
}

#[derive(Debug, Deserialize)]
pub struct LoginRequest {
    identifier: Identifier,
    password: Option<String>,
    device_id: Option<String>,
}

#[derive(Debug, Deserialize)]
#[serde(tag = "type")]
enum Identifier {
    #[serde(rename = "m.id.user")]
    Username { user: String },
}

#[derive(Serialize)]
pub struct LoginResponse {
    user_id: MatrixId,
    access_token: String,
    device_id: String,
    //TODO: This is deprecated, but Fractal is the only client that doesn't require it. Remove it
    // once all the other clients have updated to current spec
    home_server: Domain,
}

// POST /login
#[instrument(skip_all, err)]
pub async fn login(cx: Cx, Json(req): Json<LoginRequest>) -> Result<Json<LoginResponse>, Error> {
    let username = match req.identifier {
        Identifier::Username { user } => {
            let res = MatrixId::try_from(&*user);
            match res {
                Ok(mxid) => mxid.localpart().to_string(),
                Err(_) => user,
            }
        }
    };
    let password = req.password.ok_or(ErrorKind::Unimplemented)?;

    let db = cx.db_pool.get_handle().await?;
    if !db.verify_password(&username, &password).await? {
        return Err(ErrorKind::Forbidden.into());
    }

    let device_id = req
        .device_id
        .unwrap_or(format!("{:08X}", rand::random::<u32>()));
    let access_token = db.create_access_token(&username, &device_id).await?;

    tracing::info!(username = username.as_str(), "User logged in");

    let user_id = MatrixId::new(&username, cx.domain.clone()).unwrap();
    let access_token = format!("{}", access_token.hyphenated());

    Ok(Json(LoginResponse {
        user_id,
        access_token,
        device_id,
        home_server: cx.domain.clone(),
    }))
}

// POST /logout
#[instrument(skip(cx), err)]
pub async fn logout(cx: Cx, user: AuthenticatedUser) -> Result<Json<()>, Error> {
    let db = cx.db_pool.get_handle().await?;
    db.delete_access_token(user.token).await?;
    Ok(Json(()))
}

// POST /logout/all
#[instrument(skip(cx), err)]
pub async fn logout_all(cx: Cx, user: AuthenticatedUser) -> Result<Json<()>, Error> {
    let db = cx.db_pool.get_handle().await?;
    db.delete_all_access_tokens(user.token).await?;
    Ok(Json(()))
}

#[derive(Debug, Deserialize)]
pub struct RegisterRequest {
    #[serde(default)]
    username: Option<String>,
    #[serde(default)]
    password: Option<String>,
    #[serde(default)]
    device_id: Option<String>,
    #[serde(default)]
    inhibit_login: bool,
}

#[derive(Debug, Default, Deserialize)]
pub struct RegisterParams {
    kind: AccountKind,
}

#[derive(Debug, Default, Deserialize, PartialEq, Eq)]
#[serde(rename_all = "lowercase")]
enum AccountKind {
    #[default]
    User,
    Guest,
}

// POST /register
#[instrument(skip_all, fields(username = Empty), err)]
pub async fn register(
    cx: Cx,
    Query(params): Query<RegisterParams>,
    Json(req): Json<RegisterRequest>,
) -> Result<(StatusCode, Json<serde_json::Value>), Error> {
    if req.username.is_none() || req.password.is_none() {
        return Ok((
            StatusCode::UNAUTHORIZED,
            Json(json!({
                "completed": [],
                "flows": [{
                    "stages": ["m.login.dummy"],
                }]
            })),
        ));
    }
    let (username, password) = (req.username.unwrap(), req.password.unwrap());

    Span::current().record("username", &&*username);

    if params.kind != AccountKind::User {
        return Err(ErrorKind::Unimplemented.message("account type"));
    }

    let user_id = MatrixId::new(&username, cx.domain.clone()).with(ErrorKind::BadJson)?;

    let db = cx.db_pool.get_handle().await?;
    db.create_user(&user_id.localpart(), &password).await?;
    if req.inhibit_login {
        return Ok((
            StatusCode::OK,
            Json(json!({
                "user_id": username
            })),
        ));
    }

    let device_id = req
        .device_id
        .unwrap_or(format!("{:08X}", rand::random::<u32>()));
    let access_token = db
        .create_access_token(&user_id.localpart(), &device_id)
        .await?;
    let access_token = format!("{}", access_token.hyphenated());

    Ok((
        StatusCode::OK,
        Json(json!({
            "user_id": user_id,
            "access_token": access_token,
            "device_id": device_id
        })),
    ))
}

#[derive(Serialize)]
pub struct WhoamiResponse {
    user_id: MatrixId,
    is_guest: bool,
    #[serde(skip_serializing_if = "Option::is_none")]
    device_id: Option<String>,
}

// GET /_matrix/client/v3/account/whoami
pub async fn whoami(user: AuthenticatedUser) -> Result<Json<WhoamiResponse>, Error> {
    Ok(Json(WhoamiResponse {
        user_id: user.id,
        is_guest: false,
        device_id: Some(user.device_id),
    }))
}
