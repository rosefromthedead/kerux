use serde::Serialize;

use kerux_core::Error;
use kerux_extractors::Json;

use crate::{auth::AuthenticatedUser, Cx};

#[derive(Debug, Serialize)]
pub struct QueryResponse {}

// POST /keys/query
pub async fn query(
    _cx: Cx,
    _user: AuthenticatedUser,
    Json(_req): Json<serde_json::Value>,
) -> Result<Json<QueryResponse>, Error> {
    Ok(Json(QueryResponse {}))
}
