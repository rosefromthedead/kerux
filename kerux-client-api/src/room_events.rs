use futures::FutureExt;
use serde::{Deserialize, Serialize};
use serde_json::{json, Value as JsonValue};
use std::collections::HashMap;
use tokio::time::{sleep, Duration};
use tracing::instrument;

use kerux_core::{
    error::{ErrorKind, ResultExt},
    events::{
        room::{Member, Membership},
        Event, EventContent,
    },
    state::{NewEvent, StorageExt},
    storage::EventQuery,
    Error, MatrixId, RoomId,
};
use kerux_extractors::{Json, Path, Query};

use crate::{auth::AuthenticatedUser, Cx};

/// Provided in URL query params
#[derive(Debug, Default, Deserialize)]
pub struct SyncRequest {
    #[serde(default)]
    since: Option<String>,
    #[serde(default)]
    full_state: bool,
    #[serde(default)]
    timeout: u32,
}

#[derive(Debug, Default, Deserialize)]
#[serde(rename = "snake_case")]
enum SetPresence {
    Offline,
    #[default]
    Online,
    Unavailable,
}

#[derive(Debug, Serialize)]
pub struct SyncResponse {
    next_batch: String,
    rooms: Rooms,
    presence: Presence,
    account_data: AccountData,
}

#[derive(Debug, Default, Serialize)]
struct Rooms {
    join: HashMap<RoomId, JoinedRoom>,
    invite: HashMap<RoomId, InvitedRoom>,
    leave: HashMap<RoomId, LeftRoom>,
}

#[derive(Debug, Serialize)]
struct JoinedRoom {
    summary: RoomSummary,
    state: State,
    timeline: Timeline,
    ephemeral: Ephemeral,
    account_data: AccountData,
}

#[derive(Debug, Serialize)]
struct RoomSummary {
    #[serde(rename = "m.heroes")]
    heroes: Option<Vec<String>>,
    #[serde(rename = "m.joined_member_count")]
    joined_member_count: usize,
    #[serde(rename = "m.invited_member_count")]
    invited_member_count: usize,
}

#[derive(Debug, Serialize)]
struct State {
    events: Vec<Event>,
}

#[derive(Debug, Serialize)]
struct Timeline {
    events: Vec<Event>,
    limited: bool,
    prev_batch: String,
}

#[derive(Debug, Serialize)]
struct Ephemeral {
    events: Vec<KvPair>,
}

/// This is referred to as `Event` in the Matrix spec, but we already have a thing called event
/// and it doesn't really make sense to call it that.
#[derive(Debug, Serialize)]
struct KvPair {
    content: JsonValue,
    #[serde(rename = "type")]
    ty: String,
}

#[derive(Debug, Serialize)]
struct AccountData {
    events: Vec<KvPair>,
}

#[derive(Debug, Serialize)]
struct InvitedRoom {
    invite_state: InviteState,
}

#[derive(Debug, Serialize)]
struct InviteState {
    events: Vec<StrippedState>,
}

#[derive(Debug, Serialize)]
struct StrippedState {
    #[serde(flatten)]
    content: EventContent,
    state_key: String,
    sender: MatrixId,
}

#[derive(Debug, Serialize)]
struct LeftRoom {
    state: State,
    timeline: Timeline,
    account_data: AccountData,
}

#[derive(Debug, Serialize)]
struct Presence {
    events: Vec<KvPair>,
}
#[derive(Deserialize, Debug, PartialEq, Eq, Copy, Clone, Hash)]
enum MessageOrdering {
    #[serde(rename = "f")]
    Forward,
    #[serde(rename = "b")]
    Backward,
}
impl Default for MessageOrdering {
    fn default() -> Self {
        Self::Forward
    }
}

#[derive(Deserialize, Debug, Default)]
pub struct MessagesParams {
    from: Option<String>,
    filter: Option<serde_json::Value>,
    limit: Option<usize>,
    to: Option<String>,
    #[serde(default)]
    dir: MessageOrdering,
}
#[derive(Serialize, Debug)]
pub struct MessagesResponse {
    chunk: Vec<Event>,
    start: String,
}

// GET /rooms/:room_id/messages
#[instrument(skip(cx), err)]
pub async fn messages(
    cx: Cx,
    user: AuthenticatedUser,
    Path(room_id): Path<RoomId>,
    Query(params): Query<MessagesParams>,
) -> Result<Json<MessagesResponse>, Error> {
    let db = cx.db_pool.get_handle().await?;
    let filter = params.filter.as_ref().filter(|f| f.is_object());
    let query = EventQuery {
        room_id: &room_id,
        from: params
            .from
            .as_ref()
            .and_then(|p| p.parse().ok())
            .unwrap_or_default(),
        to: params.to.as_ref().and_then(|t| t.parse().ok()),
        contains_json: filter.cloned(),
        senders: &[],
        not_senders: &[],
        types: &[],
        not_types: &[],
    };
    let (mut events, _) = db.query_events(query, false).await?;
    let start = events
        .first()
        .map(|e| e.state_key.as_deref().unwrap_or("empty"))
        .unwrap_or("empty")
        .to_owned();
    if params.dir == MessageOrdering::Backward {
        events.reverse();
    }
    if params.limit.is_some() && events.len() > params.limit.unwrap() {
        let upper = events.len() - params.limit.unwrap();
        events.drain(upper..);
    }
    Ok(Json(MessagesResponse {
        chunk: events,
        start,
    }))
}

// GET /sync
#[instrument(skip(cx, req), err)]
pub async fn sync(
    cx: Cx,
    user: AuthenticatedUser,
    Query(req): Query<SyncRequest>,
) -> Result<Json<SyncResponse>, Error> {
    let db = cx.db_pool.get_handle().await?;

    let mut batch = db
        .get_batch(req.since.as_deref().unwrap_or("empty"))
        .await?
        .unwrap_or_default();
    let next_batch_id = format!("{:x}", rand::random::<u64>());
    let mut res = SyncResponse {
        next_batch: next_batch_id.clone(),
        rooms: Rooms::default(),
        presence: Presence { events: Vec::new() },
        account_data: AccountData { events: Vec::new() },
    };

    let rooms = db.get_rooms().await?;
    let mut memberships = HashMap::new();
    for room_id in rooms.iter() {
        let state = cx.state_resolver.current(room_id).await?;
        let member: Option<Member> = state.get_content(&*db, &user.id.to_string()).await?;
        if let Some(member) = member {
            memberships.insert(room_id, member.membership);
        }
    }
    let mut something_happened = false;
    for (&room_id, membership) in memberships.iter() {
        match membership {
            Membership::Join => {
                batch.invites.remove(room_id);
                let from = batch.rooms.get(room_id).map(|v| *v).unwrap_or(0);
                let (events, progress) = db
                    .query_events(
                        EventQuery {
                            from,
                            to: None,
                            room_id,
                            senders: &[],
                            not_senders: &[],
                            types: &[],
                            not_types: &[],
                            contains_json: None,
                        },
                        false,
                    )
                    .await?;
                batch.rooms.insert(room_id.clone(), progress + 1);

                let mut state_events = Vec::new();
                if req.full_state {
                    for event_id in cx.state_resolver.current(&room_id).await?.map.values() {
                        state_events.push(
                            db.get_pdu(&room_id, &event_id)
                                .await?
                                .unwrap()
                                .into_client_format(),
                        );
                    }
                }

                if !events.is_empty() || !state_events.is_empty() {
                    something_happened = true;
                }

                let (joined, invited) = cx.state_resolver.room_member_counts(&room_id).await?;
                let summary = RoomSummary {
                    heroes: None,
                    joined_member_count: joined,
                    invited_member_count: invited,
                };
                let state = State {
                    events: state_events,
                };
                let timeline = Timeline {
                    events,
                    limited: false,
                    prev_batch: String::from("empty"),
                };
                let ephemeral = Ephemeral {
                    events: db
                        .get_all_ephemeral(room_id)
                        .await?
                        .into_iter()
                        .map(|(k, v)| KvPair { ty: k, content: v })
                        .collect(),
                };
                let account_data = AccountData { events: Vec::new() };
                res.rooms.join.insert(
                    room_id.clone(),
                    JoinedRoom {
                        summary,
                        state,
                        timeline,
                        ephemeral,
                        account_data,
                    },
                );
            }
            Membership::Invite if !batch.invites.contains(room_id) => {
                let mut events = Vec::new();
                for event_id in cx.state_resolver.current(&room_id).await?.map.values() {
                    let pdu = db.get_pdu(&room_id, &event_id).await?.unwrap();
                    events.push(StrippedState {
                        content: pdu.event_content().clone(),
                        state_key: pdu.state_key().unwrap().to_owned(),
                        sender: pdu.sender().clone(),
                    });
                }
                res.rooms.invite.insert(
                    room_id.clone(),
                    InvitedRoom {
                        invite_state: InviteState { events },
                    },
                );
                batch.invites.insert(room_id.clone());
            }
            _ => {}
        }
    }

    if something_happened {
        db.set_batch(&next_batch_id, batch).await?;
        return Ok(Json(res));
    }

    let mut queries = Vec::new();
    for (&room_id, _) in memberships.iter().filter(|(_, m)| **m == Membership::Join) {
        let from = batch.rooms.get(room_id).copied().unwrap_or(0);
        let room_id_clone = room_id.clone();
        queries.push(
            db.query_events(
                EventQuery {
                    from,
                    to: None,
                    room_id,
                    senders: &[],
                    not_senders: &[],
                    types: &[],
                    not_types: &[],
                    contains_json: None,
                },
                true,
            )
            .map(move |r| (r, room_id_clone)),
        );
    }
    if queries.is_empty() {
        // user is not in any rooms. when we have a better event system we can wait for
        // invitations etc, but for now just sleep so the client doesn't sync over and over
        db.set_batch(&next_batch_id, batch).await?;
        sleep(Duration::from_millis(req.timeout as _)).await;
        return Ok(Json(res));
    }

    let timeout = sleep(Duration::from_millis(req.timeout as _));
    tokio::select! {
        _ = timeout => {
            db.set_batch(&next_batch_id, batch).await?;
            return Ok(Json(res));
        },
        ((query_res, room_id), _, _) = futures::future::select_all(queries) => {
            let (events, progress) = query_res?;
            let (joined, invited) = cx.state_resolver.room_member_counts(&room_id).await?;
            let summary = RoomSummary {
                heroes: None,
                joined_member_count: joined,
                invited_member_count: invited,
            };
            batch.rooms.insert(room_id.clone(), progress + 1);
            res.rooms.join.insert(
                room_id.clone(),
                JoinedRoom {
                    summary,
                    timeline: Timeline {
                        events,
                        limited: false,
                        prev_batch: String::from("empty"),
                    },
                    state: State { events: Vec::new() },
                    ephemeral: Ephemeral {
                        events: db.get_all_ephemeral(&room_id).await?.into_iter().map(
                            |(k, v)| KvPair {
                                ty: k,
                                content: v,
                            }).collect()
                    },
                    account_data: AccountData { events: Vec::new() },
                }
            );
            db.set_batch(&next_batch_id, batch).await?;
            return Ok(Json(res));
        },
    };
}

// GET /rooms/:room_id/event/:event_id
#[instrument(skip(cx), err)]
pub async fn get_event(
    cx: Cx,
    user: AuthenticatedUser,
    Path((room_id, event_id)): Path<(RoomId, String)>,
) -> Result<Json<Event>, Error> {
    let db = cx.db_pool.get_handle().await?;

    if cx.state_resolver.membership(&user.id, &room_id).await? != Membership::Join {
        return Err(ErrorKind::Forbidden.into());
    }

    match db.get_pdu(&room_id, &event_id).await? {
        Some(pdu) => Ok(Json(pdu.into_client_format())),
        None => Err(ErrorKind::NotFound.into()),
    }
}

// GET /rooms/:room_id/state/:event_id
pub async fn get_state_event_no_key(
    cx: Cx,
    user: AuthenticatedUser,
    Path((room_id, event_type)): Path<(RoomId, String)>,
) -> Result<Json<Event>, Error> {
    get_state_event_inner(cx, user, room_id, &event_type, "").await
}

// GET /rooms/:room_id/state/:event_id/:state_key
pub async fn get_state_event_key(
    cx: Cx,
    user: AuthenticatedUser,
    Path((room_id, event_type, state_key)): Path<(RoomId, String, String)>,
) -> Result<Json<Event>, Error> {
    get_state_event_inner(cx, user, room_id, &event_type, &state_key).await
}

#[instrument(skip(cx), err)]
pub async fn get_state_event_inner(
    cx: Cx,
    user: AuthenticatedUser,
    room_id: RoomId,
    event_type: &str,
    state_key: &str,
) -> Result<Json<Event>, Error> {
    let db = cx.db_pool.get_handle().await?;

    if cx.state_resolver.membership(&user.id, &room_id).await? != Membership::Join {
        return Err(ErrorKind::Forbidden.into());
    }

    let state = cx.state_resolver.current(&room_id).await?;
    let event_id = state.get((event_type, state_key));
    match event_id {
        Some(event_id) => Ok(Json(
            db.get_pdu(&room_id, &event_id)
                .await?
                .unwrap()
                .into_client_format(),
        )),
        None => Err(ErrorKind::NotFound.into()),
    }
}

// GET /rooms/:room_id/state
#[instrument(skip(cx), err)]
pub async fn get_state(
    cx: Cx,
    user: AuthenticatedUser,
    Path(room_id): Path<RoomId>,
) -> Result<Json<Vec<Event>>, Error> {
    let db = cx.db_pool.get_handle().await?;

    match cx.state_resolver.membership(&user.id, &room_id).await? {
        Membership::Join => {}
        _ => return Err(ErrorKind::Forbidden.into()),
    }

    let state = cx.state_resolver.current(&room_id).await?;
    let mut ret = Vec::new();
    for event_id in state.map.values() {
        ret.push(
            db.get_pdu(&room_id, &event_id)
                .await?
                .unwrap()
                .into_client_format(),
        );
    }
    Ok(Json(ret))
}

#[derive(Default, Deserialize)]
pub struct MembersRequest {
    #[serde(default)]
    membership: Option<Membership>,
    #[serde(default)]
    not_membership: Option<Membership>,
}

#[derive(Serialize)]
pub struct MembersResponse {
    chunk: Vec<Event>,
}

// GET /rooms/:room_id/members
#[instrument(skip(cx, req), err)]
pub async fn get_members(
    cx: Cx,
    user: AuthenticatedUser,
    Path(room_id): Path<RoomId>,
    Query(req): Query<MembersRequest>,
) -> Result<Json<MembersResponse>, Error> {
    let db = cx.db_pool.get_handle().await?;

    match cx.state_resolver.membership(&user.id, &room_id).await? {
        Membership::Join => {}
        _ => return Err(ErrorKind::Unimplemented.message("proper perms")),
    }

    let mut state_events = cx.state_resolver.current(&room_id).await?;
    state_events
        .map
        .retain(|(ty, _state_key), _event_id| ty == "m.room.membership");
    let mut state = Vec::new();
    for event_id in state_events.map.values() {
        state.push(
            db.get_pdu(&room_id, event_id)
                .await?
                .unwrap()
                .into_client_format(),
        );
    }
    state.retain(|event| {
        if let EventContent::Member(ref content) = &event.event_content {
            let membership = &content.membership;
            (if let Some(filter) = &req.membership {
                membership == filter
            } else {
                true
            } && if let Some(exclude) = &req.not_membership {
                membership != exclude
            } else {
                true
            })
        } else {
            false
        }
    });

    Ok(Json(MembersResponse { chunk: state }))
}

#[derive(Serialize)]
pub struct SendEventResponse {
    event_id: String,
}

// PUT /rooms/:room_id/state/:event_type/:state_key
#[instrument(skip(cx, event_content), err)]
pub async fn send_state_event(
    cx: Cx,
    user: AuthenticatedUser,
    Path((room_id, event_type, state_key)): Path<(RoomId, String, String)>,
    Json(event_content): Json<JsonValue>,
) -> Result<Json<SendEventResponse>, Error> {
    let db = cx.db_pool.get_handle().await?;

    let event = NewEvent {
        event_content: EventContent::new(&event_type, event_content).with(ErrorKind::BadJson)?,
        sender: user.id,
        state_key: Some(state_key),
        redacts: None,
        unsigned: None,
    };

    let event_id = db.add_event(&room_id, event, &cx.state_resolver).await?;

    tracing::trace!(event_id = &event_id.as_str(), "Added event");

    Ok(Json(SendEventResponse { event_id }))
}

// PUT /rooms/:room_id/send/:event_type/:txn_id
#[instrument(skip(cx, event_content), err)]
pub async fn send_event(
    cx: Cx,
    user: AuthenticatedUser,
    Path((room_id, event_type, txn_id)): Path<(RoomId, String, String)>,
    Json(event_content): Json<JsonValue>,
) -> Result<Json<SendEventResponse>, Error> {
    let db = cx.db_pool.get_handle().await?;
    if !db.record_txn(user.token, txn_id.clone()).await? {
        unimplemented!("transactions");
    }

    let event = NewEvent {
        event_content: EventContent::new(&event_type, event_content).with(ErrorKind::BadJson)?,
        sender: user.id.clone(),
        state_key: None,
        redacts: None,
        unsigned: Some(json!({ "transaction_id": txn_id })),
    };

    //TODO: is this right in the eyes of the spec? also does it matter?
    db.set_typing(&room_id, &user.id, false, 0).await?;
    let event_id = db.add_event(&room_id, event, &cx.state_resolver).await?;

    tracing::trace!(event_id = &event_id.as_str(), "Added event");

    Ok(Json(SendEventResponse { event_id }))
}
