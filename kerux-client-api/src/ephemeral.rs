use serde::Deserialize;
use serde_json::{json, Value};
use tracing::instrument;

use kerux_core::{error::ErrorKind, Error, MatrixId, RoomId};
use kerux_extractors::{Json, Path};

use crate::{auth::AuthenticatedUser, Cx};

#[derive(Deserialize)]
pub struct TypingRequest {
    typing: bool,
    #[serde(default)]
    timeout: u32,
}

// PUT /rooms/:room_id/typing/:user_id
#[instrument(skip(cx, req), err)]
pub async fn typing(
    cx: Cx,
    authed_user: AuthenticatedUser,
    Path((room_id, target_user)): Path<(RoomId, MatrixId)>,
    Json(req): Json<TypingRequest>,
) -> Result<Json<Value>, Error> {
    let db = cx.db_pool.get_handle().await?;

    if authed_user.id != target_user {
        return Err(ErrorKind::Forbidden.into());
    }
    db.set_typing(&room_id, &target_user, req.typing, req.timeout)
        .await?;
    Ok(Json(json!({})))
}
