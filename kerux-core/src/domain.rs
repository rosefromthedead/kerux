use std::str::FromStr;

use serde::{Deserialize, Serialize};

/// Matrix server domain
#[derive(Clone, Debug, PartialEq, Eq, Deserialize, Hash, PartialOrd, Ord)]
#[serde(try_from = "String")]
pub struct Domain {
    url: String,
}

impl Domain {
    pub fn new(url: String) -> Option<Self> {
        if !Self::is_valid(&url) {
            None
        } else {
            Some(Self { url })
        }
    }
    pub fn as_str(&self) -> &str {
        self.url.as_str()
    }
    pub fn is_valid(url: &str) -> bool {
        // ref https://spec.matrix.org/v1.8/appendices/#server-name
        if url.len() > 255 {
            return false;
        }

        let (host, port) = match url.rsplit_once(':') {
            Some((host, port)) => (host, Some(port)),
            None => (url, None),
        };
        if let Some(port) = port {
            if port.parse::<u16>().is_err() {
                return false;
            }
        }

        let is_ip_address = std::net::IpAddr::from_str(host).is_ok();
        let is_dns_name = host
            .chars()
            .all(|c| c.is_alphanumeric() || "-.".contains(c));
        return is_ip_address || is_dns_name;
    }
}
impl std::fmt::Display for Domain {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(&self.url)
    }
}
#[derive(Debug, PartialEq, Eq)]
pub struct InvalidDomainError {}
impl std::error::Error for InvalidDomainError {}
impl std::fmt::Display for InvalidDomainError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str("invalid domain string")
    }
}
impl Serialize for Domain {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        self.as_str().serialize(serializer)
    }
}

impl TryFrom<String> for Domain {
    type Error = InvalidDomainError;

    fn try_from(s: String) -> Result<Self, Self::Error> {
        s.parse()
    }
}
impl FromStr for Domain {
    type Err = InvalidDomainError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if !Self::is_valid(s) {
            Err(InvalidDomainError {})
        } else {
            Ok(Self { url: s.to_owned() })
        }
    }
}
#[cfg(test)]
mod tests {
    use std::str::FromStr;

    use super::Domain;

    fn check_parse(domain: &str) {
        assert_eq!(
            domain.parse(),
            Ok(Domain {
                url: domain.to_owned()
            })
        );
    }

    #[test]
    fn domain_serializes_like_a_string() {
        assert_eq!(
            serde_json::to_value(Domain::from_str("hello").unwrap()).unwrap(),
            serde_json::json!("hello")
        );
    }

    #[test]
    fn lan_domain_is_valid() {
        check_parse("something.lan")
    }
    #[test]
    fn google_dot_com_is_valid() {
        check_parse("google.com");
        check_parse("www.google.com");
    }

    #[test]
    fn domain_with_port_is_valid() {
        check_parse("thingy.com:5442");
    }
}
