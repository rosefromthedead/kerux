use std::{error::Error as StdError, fmt::Display};

use axum_core::response::{IntoResponse, Response};
use displaydoc::Display;
use http::StatusCode;
use serde_json::json;
use tracing_error::SpanTrace;

// All-seeing all-knowing error type
#[derive(Debug)]
pub struct Error {
    kind: ErrorKind,
    context: Option<Box<dyn StdError + Send + 'static>>,
    spantrace: SpanTrace,
}

impl Error {
    pub fn kind(&self) -> &ErrorKind {
        &self.kind
    }
    pub fn status_code(&self) -> StatusCode {
        use ErrorKind::*;
        match self.kind {
            Forbidden | UnknownToken | MissingToken | UsernameTaken => StatusCode::FORBIDDEN,
            NotFound | UserNotFound | RoomNotFound | UnsupportedEndpoint => StatusCode::NOT_FOUND,
            BadJson | NotJson | MissingParam | InvalidParam | UnsupportedRoomVersion => {
                StatusCode::BAD_REQUEST
            }
            LimitExceeded => StatusCode::TOO_MANY_REQUESTS,
            UnsupportedMethod => StatusCode::METHOD_NOT_ALLOWED,
            Unimplemented => StatusCode::NOT_IMPLEMENTED,
            Unknown => StatusCode::INTERNAL_SERVER_ERROR,
        }
    }
    pub fn as_json(&self) -> String {
        use ErrorKind::*;
        let errcode = match self.kind {
            Forbidden => "M_FORBIDDEN",
            UnknownToken => "M_UNKNOWN_TOKEN",
            MissingToken => "M_MISSING_TOKEN",
            BadJson => "M_BAD_JSON",
            NotJson => "M_NOT_JSON",
            NotFound | UserNotFound | RoomNotFound => "M_NOT_FOUND",
            UsernameTaken => "M_USER_IN_USE",
            LimitExceeded => "M_LIMIT_EXCEEDED",
            MissingParam => "M_MISSING_PARAM",
            InvalidParam => "M_INVALID_PARAM",
            UnsupportedRoomVersion => "M_UNSUPPORTED_ROOM_VERSION",
            UnsupportedEndpoint | UnsupportedMethod => "M_UNRECOGNIZED",
            Unimplemented | Unknown => "M_UNKNOWN",
        };
        let error = format!("{}", self);
        json!({
            "errcode": errcode,
            "error": error
        })
        .to_string()
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}\n", self.kind)?;
        if let Some(context) = &self.context {
            write!(f, "{}\n", context)?;
        }
        write!(f, "{}", self.spantrace)
    }
}

impl IntoResponse for Error {
    fn into_response(self) -> Response {
        (self.status_code(), self.as_json()).into_response()
    }
}

impl From<ErrorKind> for Error {
    fn from(kind: ErrorKind) -> Self {
        Error {
            kind,
            context: None,
            spantrace: SpanTrace::capture(),
        }
    }
}

#[derive(Debug, Display)]
pub enum ErrorKind {
    /// Forbidden access, e.g. joining a room without permission, failed login.
    Forbidden,
    /// The access token specified was not recognised.
    UnknownToken,
    /// No access token was specified for the request.
    MissingToken,
    /// The request contained valid JSON, but it was malformed in some way.
    BadJson,
    /// The request did not contain valid JSON.
    NotJson,
    /// No resource was found for this request.
    NotFound,
    /// The specified user was not found on this server.
    UserNotFound,
    /// The specified room was not found on this server.
    RoomNotFound,
    /// That username is already taken.
    UsernameTaken,
    /// Too many requests have been sent in a short period of time.
    LimitExceeded,
    /// A required URL parameter was missing from the request.
    MissingParam,
    /// A specified URL parameter has an invalid value.
    InvalidParam,
    /// The specified room version is not supported.
    UnsupportedRoomVersion,

    /// The requested endpoint is unknown to this server.
    UnsupportedEndpoint,
    /// The requested endpoint is known to this server, but not by this method.
    UnsupportedMethod,
    /// The requested feature is not implemented.
    Unimplemented,
    /// An unknown error occurred.
    Unknown,
}

impl ErrorKind {
    pub fn with<E: StdError + Send + 'static>(self, e: E) -> Error {
        Error {
            kind: self,
            context: Some(Box::new(e)),
            spantrace: SpanTrace::capture(),
        }
    }

    pub fn message(self, m: &'static str) -> Error {
        let context: Box<dyn StdError + Send + Sync> = m.to_owned().into();
        Error {
            kind: self,
            context: Some(context),
            spantrace: SpanTrace::capture(),
        }
    }
}

impl std::error::Error for Error {
    fn source(&self) -> Option<&(dyn StdError + 'static)> {
        match self.context {
            Some(ref context) => Some(&**context),
            None => None,
        }
    }
}

pub trait ResultExt<T> {
    fn with(self, kind: ErrorKind) -> Result<T, Error>;
}

impl<T, E: StdError + Send + 'static> ResultExt<T> for Result<T, E> {
    fn with(self, kind: ErrorKind) -> Result<T, Error> {
        self.map_err(|e| kind.with(e))
    }
}

// impl From<JsonPayloadError> for Error {
//     fn from(e: JsonPayloadError) -> Self {
//         if let JsonPayloadError::Deserialize(e) = e {
//             use serde_json::error::Category;
//             if e.classify() == Category::Data {
//                 ErrorKind::BadJson.with(e)
//             } else {
//                 ErrorKind::NotJson.into()
//             }
//         } else {
//             ErrorKind::Unknown.with(e)
//         }
//     }
// }
