use async_trait::async_trait;
use serde::{Deserialize, Serialize};
use serde_json::Value as JsonValue;
use std::collections::{HashMap, HashSet};
use uuid::Uuid;

use crate::{
    error::Error,
    events::{pdu::StoredPdu, room_version::VersionedPdu, Event},
    mxid::{MatrixId, RoomId},
};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct AccessTokenData {
    pub username: String,
    pub device_id: String,
}

#[derive(Clone, Debug, Default, Deserialize, Serialize)]
pub struct UserProfile {
    pub avatar_url: Option<String>,
    pub displayname: Option<String>,
}

#[derive(Clone)]
pub struct EventQuery<'a> {
    pub from: usize,
    pub to: Option<usize>,
    pub room_id: &'a RoomId,
    /// A list of event senders to include in the result. If the list is empty all senders are
    /// included.
    pub senders: &'a [&'a MatrixId],
    /// A list of event senders to exclude in the result. If the list is empty no senders are
    /// excluded. Exclusion takes priority; if a sender is listed in both `senders` and
    /// `not_senders`, the net result is exclusion.
    pub not_senders: &'a [&'a MatrixId],
    /// A list of event types to include in the result. If the list is empty all types are
    /// included.
    pub types: &'a [&'a str],
    /// A list of event types to exclude in the result. If the list is empty no types are excluded.
    /// Exclusion takes priority; if a type is listed in both `types` and `not_types`, the net
    /// result is exclusion.
    pub not_types: &'a [&'a str],
    /// Only return results whose content fields have identical values to those in here.
    pub contains_json: Option<JsonValue>,
}

impl<'a> EventQuery<'a> {
    pub fn matches(&self, pdu: &VersionedPdu) -> bool {
        if self.not_senders.contains(&pdu.sender()) {
            return false;
        }
        if !self.senders.is_empty() && !self.senders.contains(&pdu.sender()) {
            return false;
        }

        if self.not_types.contains(&pdu.event_content().get_type()) {
            return false;
        }
        if !self.types.is_empty() && !self.types.contains(&pdu.event_content().get_type()) {
            return false;
        }

        if let Some(ref value) = self.contains_json {
            let map = value.as_object().expect("contains_json must be an object");
            for (key, value) in map.iter() {
                if pdu.event_content().content_as_json().get(key) != Some(value) {
                    return false;
                }
            }
        }

        true
    }
}

#[derive(Clone, Default, Deserialize, Serialize)]
pub struct Batch {
    /// Indices into the event storage of the rooms that the user is in.
    pub rooms: HashMap<RoomId, usize>,
    /// A set of rooms to which the user has been invited, where they are already aware of this.
    pub invites: HashSet<RoomId>,
}

#[async_trait]
pub trait StorageManager: Send + Sync {
    async fn get_handle(&self) -> Result<Box<dyn Storage>, Error>;
}

#[async_trait]
pub trait Storage: Send + Sync {
    async fn create_user(&self, username: &str, password: &str) -> Result<(), Error>;

    async fn verify_password(&self, username: &str, password: &str) -> Result<bool, Error>;

    async fn create_access_token(&self, username: &str, device_id: &str) -> Result<Uuid, Error>;

    async fn delete_access_token(&self, token: Uuid) -> Result<(), Error>;

    /// Deletes all access tokens associated with the same user as this one
    async fn delete_all_access_tokens(&self, token: Uuid) -> Result<(), Error>;

    /// Returns the username for which this token is valid, if any
    async fn try_auth(&self, token: Uuid) -> Result<Option<AccessTokenData>, Error>;

    /// Records a transaction ID into the given access token and returns whether it is new
    /// (unique).
    async fn record_txn(&self, token: Uuid, txn_id: String) -> Result<bool, Error>;

    async fn get_profile(&self, username: &str) -> Result<UserProfile, Error>;

    async fn set_profile(&self, username: &str, profile: UserProfile) -> Result<(), Error>;

    async fn set_avatar_url(&self, username: &str, avatar_url: &str) -> Result<(), Error> {
        let mut profile = self.get_profile(username).await?;
        profile.avatar_url = Some(avatar_url.to_owned());
        self.set_profile(username, profile).await?;
        Ok(())
    }

    async fn set_display_name(&self, username: &str, display_name: &str) -> Result<(), Error> {
        let mut profile = self.get_profile(username).await?;
        profile.displayname = Some(display_name.to_owned());
        self.set_profile(username, profile).await?;
        Ok(())
    }

    async fn add_pdus(&self, pdus: &[StoredPdu]) -> Result<(), Error>;

    async fn get_prev_events(&self, room_id: &RoomId) -> Result<(Vec<String>, u64), Error>;

    async fn query_pdus<'a>(
        &self,
        query: EventQuery<'a>,
        wait: bool,
    ) -> Result<(Vec<StoredPdu>, usize), Error>;

    async fn query_events<'a>(
        &self,
        query: EventQuery<'a>,
        wait: bool,
    ) -> Result<(Vec<Event>, usize), Error> {
        let (pdus, next_batch) = self.query_pdus(query, wait).await?;
        return Ok((
            pdus.into_iter()
                .map(StoredPdu::into_client_format)
                .collect(),
            next_batch,
        ));
    }

    async fn get_rooms(&self) -> Result<Vec<RoomId>, Error>;

    async fn get_pdu(&self, room_id: &RoomId, event_id: &str) -> Result<Option<StoredPdu>, Error>;

    async fn get_all_ephemeral(
        &self,
        room_id: &RoomId,
    ) -> Result<HashMap<String, JsonValue>, Error>;

    async fn get_ephemeral(
        &self,
        room_id: &RoomId,
        event_type: &str,
    ) -> Result<Option<JsonValue>, Error>;

    async fn set_ephemeral(
        &self,
        room_id: &RoomId,
        event_type: &str,
        content: Option<JsonValue>,
    ) -> Result<(), Error>;

    async fn set_typing(
        &self,
        room_id: &RoomId,
        user_id: &MatrixId,
        is_typing: bool,
        timeout: u32,
    ) -> Result<(), Error>;

    async fn get_user_account_data(
        &self,
        username: &str,
    ) -> Result<HashMap<String, JsonValue>, Error>;

    async fn get_batch(&self, id: &str) -> Result<Option<Batch>, Error>;

    async fn set_batch(&self, id: &str, batch: Batch) -> Result<(), Error>;

    async fn print_the_world(&self) -> Result<(), Error> {
        Ok(())
    }
}
