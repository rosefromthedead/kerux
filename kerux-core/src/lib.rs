pub mod auth;

pub mod domain;
pub use domain::Domain;

pub mod error;
pub use error::{Error, ErrorKind};

pub mod events;

pub mod mxid;
pub use mxid::{MatrixId, RoomId};

pub mod state;

pub mod storage;

// used in tests
#[macro_export]
macro_rules! assert_ok {
    ($e:expr) => {
        if !$e.is_ok() {
            panic!(
                "assert_ok failed. expected ok but got: {err}",
                err = $e.unwrap_err()
            );
        }
    };
}
#[macro_export]
macro_rules! assert_err {
    ($e:expr) => {
        if !$e.is_err() {
            panic!("assert_err failed. expected err but got: {:?}", $e.unwrap());
        }
    };
}
